const mongoose = require("mongoose")

const courseSchema = new mongoose.Schema({

	courseName:{
		type: String,
		required: [true, "Course name is required"]
	},

	courseDescription:{
		type: String,
		required: [true, "Course description is required"]
	},

	coursePrice:{
		type: Number,
		required: [true, "Course price is required"]
	},

	isActive:{
		type: Boolean,
		default: true
	},

	createdOn:{
		type: Date,
		default: new Date()
	},

	enrollees: [{
		userID: {
			type: String,
			required: [true, "User ID is required"]
		},

		enrolledOn:{
			type: Date,
			default: new Date()
		},

		status: {
			type: String,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model("Course", courseSchema)