const User = require("./../models/Users.js")
const bcrypt = require("bcrypt")
const auth = require("./../auth")
const Course = require("./../models/Course.js")
module.exports.register= (reqBody) =>{

	return User.findOne({email: reqBody.email}).then((result,error) => {

		if(result != null){
			return `Email exists`
		}

		else{

		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10),
			mobileNo: reqBody.mobileNo
		})

		return newUser.save().then((result, error) =>{
			
			if(result){
				return true
			}else{
				return error
			}
		})
		}
	})
	
}

module.exports.getAllUsers = (reqBody) => {
	return User.find().then((result, error) =>{
		if(result){
			return result
		}else{
			return error
		}
	})
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, error) => {

		if(result == null){
			return false
		} else{
			let isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)

				}
			}
		}
	})
}

module.exports.getProfile = (userData) => {
	return User.findOne({id: userData.id}).then((result, error) => {

		
		if(result == null){
			return false
		}else{
			result.password = "";
			return result

		}
	}

	)

}

module.exports.enroll = async (data) => {
	const {userID, courseID} = data

	// look for matching document of a user

	const userEnroll = await User.findById(userID).then((result, error) => {

		console.log(result)

		if(error){
			return error
		}else{
			result.enrollments.push({courseID: courseID})

			if(result.enrollments.courseID != courseID){

			//save changes para ma update sa databse
			return result.save().then((result, error) => {
				if(error){
					return error
				}else{
					return true
				}
			})

		}else{
			return false
		}
	}
	})

	// look for matching document of a course

	const courseEnroll = await Course.findById(courseID).then((result, error) => {

		if(error){
			return error
		}else{
			result.enrollees.push({userID: userID})

			if(result.enrollees.userID != userID){
				return result.save().then((result, error) => {
					if(error){
						return error
					}else{
						return true
					}
				})
			}else{
				return false
			}
		}
	})

	if(userEnroll && courseEnroll){
		return true
	}else{
		return false
	}
}