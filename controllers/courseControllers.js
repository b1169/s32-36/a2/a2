const Course = require("./../models/Course.js")

module.exports.createCourse = (reqBody) => {

	let newCourse = new Course({

		courseName: reqBody.courseName,
		courseDescription:reqBody.courseDescription,
		coursePrice: reqBody.coursePrice
	})
	return newCourse.save().then((result, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.getAllCourses = (reqBody) => {
	return Course.find().then((result=>{
		return result
	}))
}

module.exports.getActiveCourses = (reqBody) =>{
	return Course.find({isActive:true}).then((result=>{
		return result
	}))
}

module.exports.getSpecificCourse = (reqBody) => {
	//look for the matching document in the database, if matching document found, return the document

	//if error, return error

	return Course.findOne({courseName: reqBody}).then((result, error) => {

		console.log(result)
		if(result === null){
			
			return `Course not existing`
		} else{

			if(result){

				return result
			}
			else{
				return error
			}
			
		}
	})
}

module.exports.getCourseById = (params) => {


	return Course.findById(params).then((result,error) => {

		if(result === null){
			return `Course not existing`
		} else{
			if(result){
				return result
			}else{
				return error
			}
		}
	})

}

module.exports.archiveCourse = (reqBody) => {

	let courseStatus = { isActive: false};
	
	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus, {new: true}).then((result,error) => {
		if(result == null){
			return 'Course not existing'
		}else{
				if(result){
					return true
				}else{
					return false
				}}
		
	})
}

/*module.exports.archiveCourse = (reqBody) => {

	let courseStatus = { isActive: false};
	
	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus, {new: true}).then((result,error) => {
		if(result == null){
			return 'Course not existing'
		}else{
				if(result){
					return true
				}else{
					return false
				}}
		
	})
}*/

module.exports.archiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: false}).then((result, error) => {

		if(result === null){
			return `Course not existing`
		}else{
			if(result){
				return true
			}else{
				return false
			}
		}
	})
}

module.exports.unarchiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: true}).then((result, error) => {

		if(result === null){
			return `Course not existing`
		}else{
			if(result){
				return true
			}else{
				return false
			}
		}
	})
}

module.exports.deleteCourse = (reqBody) => {

	//look for matching document and 

	return Course.findOneAndDelete({courseName: reqBody}).then((result, error) => {

		if(result === null){
			return `Course not existing`
		}else{
			if(result){
				return true
			}else{
				return error
			}
		}
	})
}

module.exports.deleteCourseById = (params) => {

	//look for matching document and 

	return Course.findByIdAndDelete(params).then((result, error) => {

		if(result === null){
			return `Course not existing`
		}else{
			if(result){
				return true
			}else{
				return error
			}
		}
	})
}

module.exports.editCourse = (id, reqBody) => {
	const {courseName, courseDescription, coursePrice} = reqBody

	let updatedCourse = {
		courseName: courseName,
		courseDescription: courseDescription,
		coursePrice: coursePrice
	}
	return Course.findByIdAndUpdate(id, updatedCourse, {new:true}).then((result, error) => {

			if(error){
				return error
			}else{
				return result
			}

	})
}