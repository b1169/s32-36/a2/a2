const express = require("express");
const router = express.Router();
const courseController = require("./../controllers/courseControllers.js")
const auth = require("./../auth")

router.post("/create-course", auth.verify, (req, res) => {

	courseController.createCourse(req.body).then(result => res.send(result))
})

router.get("/", auth.verify, (req, res) => {

	courseController.getAllCourses(req.body).then(result => res.send(result))
})

router.get("/active-courses", auth.verify, (req,res) =>{
	courseController.getActiveCourses(req.body.isActive).then(result => res.send(result))
})

//get a specific course using findOne() method

router.get("/specific-course", auth.verify, (req, res) => {

	// console.log(req.body) returns object
	courseController.getSpecificCourse(req.body.courseName).then( result => res.send(result))
})

//get specific course using findById()

router.get("/:courseID", auth.verify, (req, res) => {

	// console.log() //property of request

	courseController.getCourseById(req.params.courseID).then(result => res.send(result))
})

//update isActive status of the course using findOneAndUpdate()

	//update isActive status to false

router.put("/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.body.courseName).then(result => res.send(result))
})

	//update isActive status to true

router.put("/unarchive", auth.verify, (req, res) => {

	courseController.unarchiveCourse(req.body.courseName).then(result => res.send(result))
})
//update isActive status of the course using findByIdAndUpdate()

router.put("/:courseID/archive", auth.verify, (req, res) => {
	courseController.archiveCourseById(req.params.courseID).then(result => res.send(result))
})

router.put("/:courseID/unarchive", auth.verify, (req, res) => {
	courseController.unarchiveCourseById(req.params.courseID).then(result => res.send(result))
})

//delete course using findOneAndDelete()

router.delete("/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourse(req.body.courseName).then(result => res.send(result))
})

//delete course using findByIDAndDelete()

router.delete("/:courseID/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourseById(req.params.courseID).then(result => res.send(result))
})

router.put("/:courseID/edit", auth.verify,(req, res) => {

	courseController.editCourse(req.params.courseID, req.body).then(result => res.send)
})
module.exports = router;