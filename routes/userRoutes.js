const express = require("express")
const router = express.Router() //property of express

const auth = require("./../auth")
const userController = require("./../controllers/userControllers.js")
//http://localhost:4000/api/users

router.post("/register", (req, res) =>{
	
	userController.register(req.body).then(result => res.send(result))
})

router.get("/", (req, res) =>{

	userController.getAllUsers(req.body).then(result => res.send(result))
})

router.post("/login", (req,res) =>{
	userController.login(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify,(req, res) => {

	let userData = auth.decode(req.headers.authorization)

	console.log(userData)

	userController.getProfile(userData).then(result => res.send(result))
})

router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userID: auth.decode(req.headers.authorization).id, 
		courseID: req.body.courseID
	}

	userController.enroll(data).then(result => res.send(result))
})

module.exports = router;